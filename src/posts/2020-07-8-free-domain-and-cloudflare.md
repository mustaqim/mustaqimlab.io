---
title: Setting up a domain with Cloudflare and GitLab Pages
description: An overview on how this domain is setup
date: 2020-07-08
tags: 
  - gitlab pages
  - cloudflare
author: mustaqim
footer: Copyright © 2020-present | mustaqim
---

# Setting up a domain with Cloudflare and GitLab Pages

There are a few free domains that could be acquired via [Freenom](https://freenom.com/), such as this one.

## Setting up Cloudflare

This part is entirely optional, though re-routing through Cloudflare has quite a few benefits.

1. Log into your [Cloudflare](https://cloudflare.com/ "Cloudflare homepage") account and add the domain that was registered on Freenom.

2. Next, change the 2 DNS nameservers in the Freenom domain panel with the ones Cloudflare provides.

## Configuring DNS Records
The next step would be to configure DNS Records in Cloudflare:

![Setup A, TXT, CNAME and MX records](https://i.imgur.com/XVDUcY8.png "A, CNAME, TXT records")
At the very least, an `A Record` with the GitLab IP of `35.185.44.232`
and a `TXT Record` is required.

To setup the TXT record, copy the TXT verification code from GitLab (Settings->Pages) in the form:
`gitlab-pages-verification-code=98a231c8dd6adbcba8f445c235123b4345` in the Content Field above.

## Configuring Page Rules

Under *Page Rules*, add:

* Forward `www` to base domain

  ![Page rule to redirect www](https://i.imgur.com/kd0T3TC.png "Setup page rule to redirect www back to base domain")
  
  Make sure to setup the CNAME record pointing `www` to `mustaqim.ml` as shown above.
  
  See also: [Redirecting www.domain.com to domain.com with Cloudflare](https://docs.gitlab.com/ee/user/project/pages/custom_domains_ssl_tls_certification/#redirecting-wwwdomaincom-to-domaincom-with-cloudflare "Redirecting www.domain.com to domain.com with Cloudflare")

  This rule ensures proper redirection from `www` takes place.

::: tip Optional

## Configuring SSL

Under the SSL/TLS tab, select `Full` as the encryption level. Under Origin Server click Create Certificate and copy the PEM public and private keys
to the GitLab pages. You will also need to paste Cloudflare's Origin CA under the public key you just added. Make sure to choose either the RSA or ECDSA one from
[here](https://support.cloudflare.com/hc/en-us/articles/115000479507 "Cloudflare Origin CA")
:::

## Email forwarding

You could forward emails sent to your custom domain to your email address. An example of such a service is [ImprovMX](https://improvmx.com/). This is not required if you setup your own email server.

