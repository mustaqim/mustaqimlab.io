---
title: Using Vim effectively
description: A quick guide to using Vim to manipulate text
date: 2017-11-22
tags:
  - vim
  - terminal
  - text editor
author: mustaqim
---

# Using Vim effectively

[**My current nvim setup**](https://github.com/mustaqimM/dotfiles/blob/master/.config/nvim/init.vim)
![vim preview](https://camo.githubusercontent.com/d163ed6ea53ac7b5b4c5ff264419221d5957da9d/68747470733a2f2f692e696d6775722e636f6d2f3555786e3971462e706e67 "vim window preview")

Vim is a very powerful text editor that makes manipulating text much easier
and faster once it's commands are embedded into memory.
The easiest way of understanding the core aspects of using vim would be to use
it's built in tutorial. Run


```vim
:tutor
```

and complete it. Re-run it as many times as necessary.



### Normal Commands

- ```i```		- insert mode
- ```u```		- undo last
- ```U```		- Undo line
- ```dd```		- delete(and copy) line
- ```p```		- paste
- ```gg```		- move cursor to first line
- ```42G```		- move to start of line 42
- ```dG```		- delete all lines
- ```g=GG```	- indent all lines
- ```gg"+yG```	- copy all lines (`+`: to system clipboard)
- ```:%y+```	- copy all lines (`%`: affect all lines`)

### Commands (invoked with ```:```)

- ```:q```				- quit
- ```:q!``` 			- quit, discarding changes
- ```:w```  			- save
- ```:wq``` 			- save and quit
- ```:x```  			- save and quit
- ```:r !xsel -b```		- copy clipboard contents to file

To save a file (:w) that was opened as Read-only:

```
:w !sudo tee %
```

### Registers

- ```:reg```	- view registers
- ```".```	- last entered text
- ```"%```	- current file path
- ```":```	- most recently executed command
