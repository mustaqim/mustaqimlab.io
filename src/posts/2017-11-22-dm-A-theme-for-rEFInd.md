---
title: dm - rEFInd theme
description: "A dark theme for rEFInd"
date: 2017-11-22
tags:
  - linux
  - rEFInd
author: mustaqim
---

# dm
A dark theme for [rEFInd](http://www.rodsbooks.com/refind/) based on [Dream Machine](https://github.com/Lindstream/dm-refind-theme).

![dm preview](https://github.com/mustaqimM/dm/raw/master/screenshot.png "dm preview")

## Installation
```
$root: cp -r ~/dm /boot/efi/EFI/refind/themes
$root: nvim + /boot/efi/EFI/refind/refind.conf # Opens nvim and puts the cursor on the last line
$root: # Append "include themes/dm/theme.conf" to `refind.conf`
```

## NOTES

Obviously [rEFInd](http://www.rodsbooks.com/refind/) is needed to use this. Install with your package manager `refind-efi`. You many need to run `refind-install` afterwards.

If the `refind` bootloader doesn't show up on boot even after running `refind-install`. Run `efibootmgr` (to see the current boot order), then set the order so it is loaded first, for example: `efibootmgr -o 0002,0003,0001`. You may first need to clear the boot order: `efibootmgr -O` OR delete a specific entry: `efibootmgr -A 0002`

Attributions

**Font:** [Dento font](http://fontmeme.com/freefonts/34867/dento.font)

**Icons:** [Lightness for burg](http://sworiginal.deviantart.com/art/Lightness-for-burg-181461810) & [refind-ambience](https://github.com/lukechilds/refind-ambience).

**Reddit:** [rEFInd](http://www.rodsbooks.com/refind/)


<h5 align="center">
  <img width="150" src="https://i.imgur.com/rHXKOj8.png" alt="bitcoin">
  <FIGCAPTION><center><a href="bitcoin:bc1qn8v03jcds60qyszlcg0sxet7efem2lkf7jucyg">bitcoin:bc1qn8v03jcds60qyszlcg0sxet7efem2lkf7jucyg</a></center></FIGCAPTION>
</h5>
