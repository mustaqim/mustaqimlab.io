---
sidebar: false
---

<About title="about me" />

I am a student currently pursuing Computer and Data Science. I have experience developing and designing software for android, linux and the web;
from simple landing pages to PWAs. The languanges I'm familiar with are Java, Python, shell, VueJs.
I strive to create software that not only functions efficiently under the hood, but also provides intuitive,
pixel-perfect user experiences.
<br /><br />
I enjoy learning new and better ways to create seamless user experiences with clean, efficient, and scalable code.
I consider my work an ongoing learning experience and I'm always looking for opportunities to work with those who are willing to
share their knowledge as much as I want to learn. My primary goal is to create projects that are simple, useful and visually
pleasing to those that use it.
