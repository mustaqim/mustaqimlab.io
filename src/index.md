---
home: true
heroImage: /m.svg
herotext: mustaqim malim
tagline:
actionText: latest posts 🢡
actionLink: /posts/
footer: Copyright © 2021 | mustaqim malim
---

<br />

<div class="features">
  <div class="feature">
    <h2>Computer Science</h2>
    <p>University of South Africa</p>
  </div>
  <div class="feature">
    <h2>Data Science</h2>
    <p>Machine Learning with fastai, pytorch & jupyter</p>
  </div>
  <div class="feature">
    <h2>Mobile & Web Development</h2>
    <p>Android with Kotlin; web with VueJS</p>
  </div>
</div>
