module.exports = {
    title: 'mustaqim malim',
    description: 'Personal blog for Mustaqim Malim',
    // base: '/site/',
    dest: 'public',
    theme: 'default-prefers-color-scheme',
    // theme: '@vuepress/theme-blog',
    themeConfig: {
        dateFormat: 'DD-MM-YYYY',
        // footer: {
        //     contact: [
        //         {
        //             type: 'gitlab',
        //             link: 'https://gitlab.com/mustaqim/',
        //         }
        //     ],
        // },
        // plugins: ['@vuepress/blog'],
        // defaultTheme: 'dark',
        logo: '/m.svg',
        // head: [
        //     ['link', { rel: 'icon', href: '/m.svg' }]
        // ],
        nav: [
            // { text: 'Home', link: '/' },
            { text: 'posts', link: '/posts/' },
            {
                text: 'extended topics',
                items: [
                    { text: 'Machine Learning', link: '/machine-learning/' }
                ]
            },
            // { text: 'projects', link: '/projects/' },
            { text: 'tags', link: '/tags/' },
            { text: 'about', link: '/about/' },
            // { text: 'code', link: 'https://gitlab.com/mustaqim/site' },
            // {
            //     text: 'Languages',
            //     ariaLabel: 'Language Menu',
            //     items: [
            //         { text: 'Chinese', link: '/language/chinese/' },
            //         { text: 'Japanese', link: '/language/japanese/' }
            //     ]
            // }
        ],
        sidebar: {
            '/machine-learning/': [
                {
                    title: 'Deep Learning with fast.ai',
                    collapsable: false,
                    children: [
                        '',
                        'setting-up-a-server',
                        'calculating-gradients',
                    ]
                }
            ],
            '/posts/': 'auto',
            // fallback
            '/': [
                '',        /* / */
                // 'contact', /* /contact.html */
                // 'about'    /* /about.html */
            ]
        },
        // displayAllHeaders: true, // Default: false
        // activeHeaderLinks: false, // Default: true
        // sidebar: [
        //     '/',
        //     '/posts/guide',
        //     ['/page-b', 'Explicit link text']
        // ],
        // lastUpdated: 'Last Updated', // string | boolean
        smoothScroll: false,
        // searchPlaceholder: 'Search...',

        // repo: 'https://gitlab.com/mustaqim/mustaqim.gitlab.io',
        // Customising the header label
        // Defaults to "GitHub"/"GitLab"/"Bitbucket" depending on `themeConfig.repo`
        // repoLabel: 'Contribute!',
        editLinks: true,
        editLinkText: 'Help me improve this page!',
        // nextLinks: true,
        // prevLinks: true,
    },
    markdown: {
        lineNumbers: true
    },

    // plugins: ['vuepress-plugin-reading-time', {
    //     excludes: ['/about', '/tag/.*']
    // }],
   
    // module: {
    //     rules: [
    //         // ... other rules omitted

    //         // this will apply to both plain `.scss` files
    //         // AND `<style lang="scss">` blocks in `.vue` files
    //         {
    //             test: /\.scss$/,
    //             use: [
    //                 'vue-style-loader',
    //                 'css-loader',
    //                 'sass-loader'
    //             ]
    //         }
    //     ]
    // },

    // chainWebpack(config, isServer) {
    //     for (const lang of ["sass", "scss"]) {
    //         for (const name of ["modules", "normal"]) {
    //             const rule = config.module.rule(lang).oneOf(name);
    //             rule.uses.delete("sass-loader");

    //             rule
    //                 .use("sass-loader")
    //                 .loader("sass-loader")
    //                 .options({
    //                     implementation: require("sass"),
    //                     sassOptions: {
    //                         fiber: require("fibers"),
    //                         indentedSyntax: lang === "sass"
    //                     }
    //                 });
    //         }
    //     }
    // }
}
