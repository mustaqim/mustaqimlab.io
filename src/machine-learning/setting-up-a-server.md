# Setting up a server

There are 2 services that I tested, each with their weaknesses. [Paperspace](https://paperspace.com/ "paperspace.com") & [Google Colab](https://colab.research.google.com/ "colab.research.google.com").

## Paperspace
* is a native jupyter experience.
* on the Free Tier, it provides a very generous 6 hour machine time. Though it often enough isn't available.

## Colab
* has a feature where `CTRL` + clicking on a method opens up the source code in a window.
