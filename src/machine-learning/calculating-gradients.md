# Calculating gradients

```python
xt = tensor([3.,4.,10.]).requires_grad_()
xt

---------------------------------
tensor([ 3.,  4., 10.], requires_grad=True)
```

The `requires_grad_()` method tells PyTorch to *tag* that variable at that value so that we can calculate its gradient at a later time.

```python
def f(x): return x**2

yt = f(xt)
yt
_________________________________
tensor(9., grad_fn=<PowBackward0>)
```

### Calculate the gradient
```python
yt.backward()
```

### View the gradient by accessing the `grad` property
```python
xt.grad
```
